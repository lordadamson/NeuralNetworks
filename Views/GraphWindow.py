import matplotlib.pyplot as plt


class GraphWindow():
	X1 = []
	X2 = []
	Y1 = []
	Y2 = []

	def __init__(self, p_x1, p_y1, p_x2, p_y2):
		self.X1 = p_x1
		self.X2 = p_x2
		self.Y1 = p_y1
		self.Y2 = p_y2
		self.plot()

	def plot(self):
		plt.plot(self.X1, self.Y1, 'bs', self.X2, self.Y2, 'g^')
		plt.plot([3,8], [4,6])
		plt.show()