#!/usr/bin/python3

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from neural_networks.Controllers.NeuralNetwork import *

if __name__ == '__main__':
	NN = NeuralNetwork()