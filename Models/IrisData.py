class IrisData():
	path = "Models/iris_data"
	iris_data = []

				# X1, X2, X3, X4
	iris_setosa = [[], [], [], []]
	iris_versicolor = [[], [], [], []]
	iris_virginica = [[], [], [], []]

	def __init__(self):
		file = open(self.path, 'r')
		self.iris_data = file.readlines()
		file.close()
		for i in range(len(self.iris_data)):
			self.iris_data[i] = self.iris_data[i].split(',')
			if self.iris_data[i][4] == 'Iris-setosa\n':
				for j in range(4):
					self.iris_setosa[j].append(self.iris_data[i][j])
			elif self.iris_data[i][4] == 'Iris-versicolor\n':
				for j in range(4):
					self.iris_versicolor[j].append(self.iris_data[i][j])
			elif self.iris_data[i][4] == 'Iris-virginica\n':
				for j in range(4):
					self.iris_virginica[j].append(self.iris_data[i][j])
