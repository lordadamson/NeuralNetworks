from neural_networks.Models.IrisData import *
from neural_networks.Views.GraphWindow import *


class NeuralNetwork():
	iris_data = None
	MainWindow = None

	def __init__(self):
		self.iris_data = IrisData()
		self.MainWindow = GraphWindow(self.iris_data.iris_setosa[0], self.iris_data.iris_setosa[1],
									  self.iris_data.iris_versicolor[0], self.iris_data.iris_versicolor[1])

	